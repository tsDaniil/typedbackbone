/// <reference path='../signal/RootSignal.ts' />
/// <reference path='../signal/Signal.ts' />

module TypedBackbone {
    'use strict';

    export abstract class Model<Schema extends Object, SignalSchema extends IBaseRootSchema> {

        public attributes: Schema;
        public change: IRootSignal<SignalSchema>;

        constructor(attributes?: Schema) {

            this.attributes = attributes || this.defaults();
            this.initSchema();

        }

        public abstract defaults(): Schema;

        private initSchema(): void {

            let signals: SignalSchema = <any>{};

            Object.keys(this.attributes).forEach((key: string): void => {

                let value = this.attributes[key];
                signals[key] = new TypedBackbone.Signal();

                Object.defineProperty(this.attributes, key, {
                    get: function (): any {
                        return value;
                    },
                    set: function (newValue: any): void {
                        value = newValue;
                        signals[key].trigger(newValue);
                    }
                });

            });

            this.change = new TypedBackbone.RootSignal(signals);

        }

    }

}
