/// <reference path='./signals.d.ts' />
/// <reference path='./Signal.ts' />

module TypedBackbone {
    'use strict';

    export class RootSignal<SignalsSchema extends IBaseRootSchema>
    extends Signal<any> implements IRootSignal<SignalsSchema> {

        public children: SignalsSchema;

        constructor(signals: SignalsSchema) {
            super();
            this.initSchema(signals);
        }



        private initSchema(signals: SignalsSchema): void {

            this.children = signals;

            Object.keys(signals).forEach((name: string) => {

                let handler = () => {
                    this.trigger(name);
                };

                ['on', 'once', 'off'].forEach((method: string): void => {
                    RootSignal.replaceMethod(method, signals[name], handler);
                });

                signals[name].on(handler);
            });

        }

        private static replaceMethod(method: string, signal: any, handler: Function): void {

            let origin = signal[method];

            signal[method] = function (...args: Array<any>): any {
                origin.apply(signal, args);
                if (method === 'off') {
                    if (!signal.signals.length) {
                        signal.on(handler);
                    }
                } else {
                    signal.signals.sort((a: any, b: any): number => {
                        return a.callback === handler ? 1 : b.callback === handler ? -1 : 0;
                    });
                }
            };

        }

    }

}
