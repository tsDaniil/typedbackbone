declare module TypedBackbone {

    interface IBaseRootSchema {
        [key: string]: ISignal<any>;
    }

    interface ISignalCallback<T> {
        (data: T): any;
    }

    export interface ISignal<T> {
        on(callback: ISignalCallback<T>, context?: any): ISignal<T>;
        once(callback: ISignalCallback<T>, context: any): ISignal<T>;
        off(callback?: ISignalCallback<T>): ISignal<T>;
        trigger(data: T): ISignal<T>;
    }

    export interface IRootSignal<Shcema extends IBaseRootSchema> extends ISignal<string> {
        children: Shcema;
    }

}
