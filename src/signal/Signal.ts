/// <reference path='./signals.d.ts' />

module TypedBackbone {
    'use strict';

    export class Signal<T> implements ISignal<T> {

        private signals: Array<ISignalData<T>> = [];

        public on(callback: ISignalCallback<T>, context?: any): ISignal<T> {

            this.signals.push({
                context: context,
                isOnce: false,
                callback: callback
            });

            return this;
        }

        public once(callback: ISignalCallback<T>, context: any): ISignal<T> {

            this.signals.push({
                context: context,
                isOnce: true,
                callback: callback
            });

             return this;
        }

        public off(callback?: ISignalCallback<T>): ISignal<T> {

            this.signals = this.signals.filter((signalInfo: ISignalData<T>): boolean => {
                return !(!callback || callback === signalInfo.callback);
            });

            return this;
        }

        public trigger(data: T): ISignal<T> {

            this.signals.slice().forEach((signalInfo: ISignalData<T>) => {
                signalInfo.callback.call(signalInfo.context || this, data);
            });

            return this;
        }

    }

    interface ISignalData<T> {
        context: any;
        isOnce: boolean,
        callback: (data: T) => any;
    }
}
