/// <reference path='src/signal/signals.d.ts' />
/// <reference path='src/signal/RootSignal.ts' />
/// <reference path='src/model/Model.ts' />

let mySignal: TypedBackbone.ISignal<string> = new TypedBackbone.Signal();

mySignal.on((some: string) => {
    console.log(some);
});

mySignal.trigger('1');

interface IMySchema extends TypedBackbone.IBaseRootSchema {
    time: TypedBackbone.ISignal<Date>;
    age: TypedBackbone.ISignal<number>;
}

let myRootSignal: TypedBackbone.RootSignal<IMySchema> = new TypedBackbone.RootSignal({
    'time': new TypedBackbone.Signal(),
    'age': new TypedBackbone.Signal()
});

myRootSignal.children.time.on((time: Date) => {
    console.log(time.toDateString());
});

myRootSignal.children.age.on((age: number) => {
    console.log(age);
});

myRootSignal.on((childSignalName: string) => {
    console.log(childSignalName);
});

myRootSignal.children.age.trigger(22);
myRootSignal.children.time.trigger(new Date());

myRootSignal.children.age.off();
myRootSignal.children.age.trigger(45);

interface IAttrs {
    a: number;
    b: string;
}

interface ISignals extends TypedBackbone.IBaseRootSchema {
    a: TypedBackbone.ISignal<number>;
    b: TypedBackbone.ISignal<string>;
}

class MyModel extends TypedBackbone.Model<IAttrs, ISignals> {

    public defaults(): IAttrs {
        return {
            a: 1,
            b: '1'
        };
    }

}

let myModel = new MyModel({
    a: 2,
    b: '2'
});

myModel.change.children.a.on((val: number): void => {
    console.log('change: a!', val);
});

myModel.change.children.b.on((val: string): void => {
    console.log('change: b!', val);
});

myModel.change.on((name: string): void => {
    console.log('change: ' + name);
});

console.log(myModel.attributes.a);
console.log(myModel.attributes.b);

myModel.attributes.a = 22;
myModel.attributes.b = 'some';

console.log(myModel.attributes.a);
console.log(myModel.attributes.b);
